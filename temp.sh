#!/bin/bash

old_IFS=$IFS
IFS=$'\n'

filecontent=( `cat "$1" ` )

for aline in "${filecontent[@]}"
do
  echo "$aline"
  thesong=`/usr/bin/getsonginfo.sh "$aline"`
  echo /usr/bin/getsonginfo.sh "$aline"
  echo "$thesong"
  songlist="$songlist, then $thesong"  
  echo "--------"
done

IFS=$old_IFS

pico2wave -l en-GB -w /tmp/out.wav "$songlist"
lame /tmp/out.wav /tmp/out.mp3

exit 0

