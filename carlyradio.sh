#!/bin/bash

radioname="carly"
databasefile="/home/liquidsoap/${radioname}radio.db"

maxtracks=4
# 15 minutes
maxlenseconds=900
maxdailyplays=1

tempdir='/tmp'
tempprefix="$tempdir/$radioname"

collections=("/data/Carlys Music")

