#!/bin/sh

song="$1"


title=`id3info "$song" | grep -a "(Title/songname/content description)" | cut -d\: -f2 | sed -e 's/^[[:space:]]*//g' -e 's/[[:space:]]*\$//g'`
artist=`id3info "$song" | grep -a "(Lead performer(s)/Soloist(s))" | cut -d\: -f2 | sed -e 's/^[[:space:]]*//g' -e 's/[[:space:]]*\$//g'`
album=`id3info "$song" | grep -a "(Album/Movie/Show title)" | cut -d\: -f2 | sed -e 's/^[[:space:]]*//g' -e 's/[[:space:]]*\$//g'`
track=`id3info "$song" | grep -a "(Track number/Position in set)" | cut -d\: -f2 | sed -e 's/^[[:space:]]*//g' -e 's/[[:space:]]*\$//g'`

echo "$title, by $artist, from the album $album. "

