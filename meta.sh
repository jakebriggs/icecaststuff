#!/bin/sh

# Example metadata script that has the behavior required by ezstream.

#test -z "${1}" && echo "Ezstream presents"
#test x"${1}" = "xartist" && echo "Great Artist"
#test x"${1}" = "xtitle" && echo "Great Song"

#song=`cat /tmp/song`
song=`tail -n 1 /tmp/song`


title=`id3info "$song" | grep -a "(Title/songname/content description)" | cut -d\: -f2 | sed -e 's/^[[:space:]]*//g' -e 's/[[:space:]]*\$//g'`
artist=`id3info "$song" | grep -a "(Lead performer(s)/Soloist(s))" | cut -d\: -f2 | sed -e 's/^[[:space:]]*//g' -e 's/[[:space:]]*\$//g'`
album=`id3info "$song" | grep -a "(Album/Movie/Show title)" | cut -d\: -f2 | sed -e 's/^[[:space:]]*//g' -e 's/[[:space:]]*\$//g'`
track=`id3info "$song" | grep -a "(Track number/Position in set)" | cut -d\: -f2 | sed -e 's/^[[:space:]]*//g' -e 's/[[:space:]]*\$//g'`

test -z "${1}" && echo "$track - $title: $artist - $album"

#test -z "${1}" && echo $album | sed -e 's/^[[:space:]]*//g' -e 's/[[:space:]]*\$//g'
test x"${1}" = "xartist" && echo $artist | sed -e 's/^[[:space:]]*//g' -e 's/[[:space:]]*\$//g' 
test x"${1}" = "xtitle" && echo $title | sed -e 's/^[[:space:]]*//g' -e 's/[[:space:]]*\$//g'


#echo "$track $title - $artist, $album" 

