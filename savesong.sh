#!/bin/bash
databasefile=/home/liquidsoap/jakeradio.db

#databasefile=/home/jake/jakeradio.db

# make the database if it doesn't exist
if [ ! -e "$databasefile" ]
then
  cat /dev/null > "$databasefile"
  /usr/bin/sqlite3 "$databasefile" "create table songs (id INTEGER PRIMARY KEY,filepath TEXT)"
  /usr/bin/sqlite3 "$databasefile" "create table played (id INTEGER PRIMARY KEY, songid INTEGER, playedtimestamp DATETIME DEFAULT CURRENT_TIMESTAMP, FOREIGN KEY(songid) REFERENCES songs(id))"
fi

filename=`echo "$1" | sed "s/'/''/g"`

songid=`sqlite3 $databasefile "SELECT id FROM songs WHERE '$filename' =  filepath"`

if [ "$songid" == "" ]
then
  sqlite3 $databasefile "insert into songs (filepath) values ('$filename')"
  songid=`sqlite3 $databasefile "SELECT id FROM songs WHERE '$filename' =  filepath"`
fi
  
sqlite3 $databasefile "insert into played (songid) values ($songid)"

