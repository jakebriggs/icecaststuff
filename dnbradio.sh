#!/bin/bash

radioname="dnb"
databasefile="/home/liquidsoap/${radioname}radio.db"

maxtracks=4
# 10 hours
maxlenseconds=36000

tempdir='/tmp'
tempprefix="$tempdir/$radioname"

collections=("/data/music/Mixes")

maxdailyplays=1
